package ru.spbstu.icc.ics;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.hadoop.hive.HiveClient;
import org.springframework.data.hadoop.hive.HiveClientFactory;
import org.springframework.data.hadoop.hive.HiveTemplate;
import ru.spbstu.icc.ics.init.Initializer;
import ru.spbstu.icc.ics.model.hive.Tweet;
import ru.spbstu.icc.ics.repository.TweetRepository;

import java.time.LocalTime;
import java.util.List;

/**
 * Analyzer application.
 * <p/>
 * Created by Nina Lukashina on 11.12.2015.
 */
public class AnalyzerApp {

    public static void main(String[] args) {
        AbstractApplicationContext context =
            new ClassPathXmlApplicationContext("/META-INF/spring/application-context.xml",
                AnalyzerApp.class);
        context.registerShutdownHook();

        System.out.println("----Start of analyzing----");

        HiveTemplate template = context.getBean(HiveTemplate.class);
        /*template.query("drop table tweets");

        System.out.println("----Create tweets table if not exists----");

        Initializer.init(context);
        Initializer.loadData();*/
        //Initializer.addTestData(context);
        System.out.println("Country analyzing...");
        //List<String> result=template.query("SELECT country,COUNT(DISTINCT country) FROM tweets GROUP BY country");
        List<String> result=template.query("SELECT country,createdAt FROM tweets");
        for (int i=0; i<result.size(); i++)
            System.out.println(result.get(i));
        System.out.println("Followers analyzing...");

        //result=template.query("SELECT followers_count,COUNT(DISTINCT followers_count) FROM tweets GROUP BY followers_count");
        for (int i=0; i<result.size(); i++)
            System.out.println(result.get(i));
        System.out.println("----End of analyzing----");
    }
}
