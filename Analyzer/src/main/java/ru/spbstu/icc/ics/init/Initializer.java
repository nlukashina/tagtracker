package ru.spbstu.icc.ics.init;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.hadoop.hive.HiveClient;
import org.springframework.data.hadoop.hive.HiveClientFactory;
import org.springframework.data.hadoop.hive.HiveTemplate;
import ru.spbstu.icc.ics.model.hive.Tweet;
import ru.spbstu.icc.ics.rabbit.OnDataCallback;
import ru.spbstu.icc.ics.rabbit.RabbitAdapterReceiver;
import ru.spbstu.icc.ics.rabbit.RabbitAdapterSender;
import ru.spbstu.icc.ics.repository.TweetRepository;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeoutException;

/**
 * Initializer.
 * <p/>
 * Created by Nina Lukashina on 13.12.2015.
 */
public final class Initializer {
    private static final String initQuery =
        "create table if not exists tweets (id string, createdAt Date, country String, "
            + "retweet_count INT,"
            + "favorite_count INT, lang String, followers_count INT, text String, "
            + "hashtags String) "
            + "ROW FORMAT DELIMITED FIELDS TERMINATED BY ':' LINES TERMINATED BY '10'";
    private static final List<Tweet> loadedTweets = new ArrayList<>();
    private static AbstractApplicationContext context =
        new ClassPathXmlApplicationContext("/META-INF/spring/application-context.xml",
            Initializer.class);

    public static void init(final AbstractApplicationContext context) {
        HiveTemplate template = context.getBean(HiveTemplate.class);
        template.query(initQuery);
    }

    public static void addTestData() {
        HiveClientFactory hiveClientFactory = context.getBean(HiveClientFactory.class);
        HiveClient hiveClient = hiveClientFactory.getHiveClient();
        long numOfSavedTweets =
            Long.parseLong(hiveClient.executeAndfetchOne("select count(*) from tweets"));
        if (numOfSavedTweets == 0) {
            Tweet tweet1 = new Tweet();
            tweet1.setId("1");
            tweet1.setCreated_at(LocalDateTime.now());
            tweet1.setCountry("Russia");
            tweet1.setFavouriteCount(2);
            tweet1.setLang("RU");
            tweet1.setRetweetCount(1);
            tweet1.setUserFollowersCount(4);

            Tweet tweet2 = new Tweet();
            tweet2.setId("2");
            tweet2.setCreated_at(LocalDateTime.now());
            tweet2.setCountry("USA");
            tweet2.setFavouriteCount(0);
            tweet2.setLang("EN");
            tweet2.setRetweetCount(0);
            tweet2.setUserFollowersCount(0);

            Tweet tweet3 = new Tweet();
            tweet3.setId("3");
            tweet3.setCreated_at(LocalDateTime.now());
            tweet3.setCountry("France");
            tweet3.setFavouriteCount(1);
            tweet3.setLang("FR");
            tweet3.setRetweetCount(2);
            tweet3.setUserFollowersCount(2);

            TweetRepository repository = context.getBean(TweetRepository.class);
            repository.save(Arrays.asList(tweet1, tweet2, tweet3));

            numOfSavedTweets =
                Long.parseLong(hiveClient.executeAndfetchOne("select count(*) from tweets"));
            if (numOfSavedTweets > 0) {
                System.out.println("Test data added to database. ");
            } else {
                System.out.println("Error during adding test data to database. ");
            }
        } else {
            System.out.println("Database has test data. ");
        }
    }

    public static void loadData() {
        RabbitAdapterReceiver receiver = null;
        RabbitAdapterSender sender = null;
        try {
            receiver = new RabbitAdapterReceiver(callback);
            sender = new RabbitAdapterSender();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        try {
            sender.sendMessage("{type:1}");
            receiver.receiveMessages();
        } catch (IOException e) {
        }
    }

    static OnDataCallback callback = new OnDataCallback() {

        public void saveData(String message) {
            System.out.println(message);
            if (message.equals("{\"type\":2}")) {
                System.out.println("All tweets loaded. Start adding them to Hive.");
                TweetRepository repository = context.getBean(TweetRepository.class);
                Tweet tweet = Initializer.loadedTweets.get(0);
                repository.save(Arrays.asList(tweet));
                for (int k = 1; k < 719; k++) {
                    List<Tweet> tweetsPart =
                        Initializer.loadedTweets.subList(k * 100, k * 100 + 100);
                    repository.save(tweetsPart);
                    System.out.println("Saved portion of tweets: " + k + " of 719 (+37).");
                }
                repository.save(Initializer.loadedTweets.subList(71800, 71838));

                return;
            }

            try {
                JSONObject messageJson = new JSONObject(message);
                try {
                    int type = messageJson.getInt("type");

                    if (type == 2) {
                        JSONArray tweets = messageJson.getJSONArray("tweets");
                        for (int j = 0; j < tweets.length(); j++) {
                            JSONObject tweetJson = tweets.getJSONObject(j);

                            String id = tweetJson.getString("id");
                            String createdAtString = tweetJson.getString("createdAt");
                            //createdAtString = createdAtString.substring(4);
                            final String twitterFormat = "EEE MMM dd HH:mm:ss ZZZZZ yyyy";

                            SimpleDateFormat sf =
                                new SimpleDateFormat(twitterFormat, Locale.ENGLISH);
                            sf.setLenient(true);
                            Date createdAtDate = sf.parse(createdAtString);
                            Instant instant = Instant.ofEpochMilli(createdAtDate.getTime());
                            LocalDateTime createdAt =
                                LocalDateTime.ofInstant(instant, ZoneId.systemDefault());


                            String country = tweetJson.getString("country");
                            int retweetCount = tweetJson.getInt("retweet_count");
                            int followersCount = tweetJson.getInt("followers_count");
                            String lang = tweetJson.getString("lang");
                            int favoriteCount = tweetJson.getInt("favorite_count");
                            String text = tweetJson.getString("text");
                            JSONArray hashtagsArray = tweetJson.getJSONArray("hashtags");
                            List<String> hashtags = new ArrayList<>();
                            for (int i = 0; i < hashtagsArray.length(); i++) {
                                hashtags.add(hashtagsArray.getJSONObject(i).getString("text"));
                            }

                            Tweet tweet =
                                new Tweet(id, createdAt, country, retweetCount, favoriteCount, lang,
                                    followersCount, text, hashtags);
                            loadedTweets.add(tweet);
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } catch (JSONException ex) {
                ex.printStackTrace();
                return;
            }
        }

    };
}
