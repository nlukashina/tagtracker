package ru.spbstu.icc.ics.repository;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.hadoop.hive.HiveOperations;
import org.springframework.stereotype.Repository;
import ru.spbstu.icc.ics.model.hive.Tweet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Repository for tweets.
 * <p/>
 * Created by Nina Lukashina on 12.12.2015.
 */
@Repository public class TweetRepository {

    private @Value("${hive.table}") String tableName;

    private HiveOperations hiveOperations;

    @Autowired public TweetRepository(HiveOperations hiveOperations) {
        this.hiveOperations = hiveOperations;
    }

    public void save(final Tweet tweet) {
        if (tweet == null) {
            return;
        }
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", tweet.getId());
        parameters.put("createdAt", tweet.getCreated_at());
        parameters.put("country", tweet.getCountry());
        parameters.put("retweet_count", tweet.getRetweetCount());
        parameters.put("favorite_count", tweet.getFavouriteCount());
        parameters.put("lang", tweet.getLang());
        parameters.put("followers_count", tweet.getUserFollowersCount());
        hiveOperations.query("INSERT INTO TABLE " + tableName
            + " VALUES ('${hiveconf:id}', '${hiveconf:createdAt}', '${hiveconf:country}', "
            + "'${hiveconf:retweet_count}', "
            + "'${hiveconf:favorite_count}', '${hiveconf:lang}', '${hiveconf:followers_count}', "
            + "'${hiveconf:text}', '${hiveconf:hashtags'", parameters);
    }

    public void save(final List<Tweet> tweets) {

        if (CollectionUtils.isEmpty(tweets)) {
            System.out.println("List of tweets is empty. ");
            return;
        }
        Map<String, Object> parameters = new HashMap<>();
        String query = "INSERT INTO TABLE " + tableName + " VALUES ";
        for (int i = 0; i < tweets.size(); i++) {
            query += "(";
            Tweet tweet = tweets.get(i);
            String id = "id" + i;
            String idStr = "'${hiveconf:" + id + "}'";
            parameters.put(id, tweet.getId());

            String createdAt = "createdAt" + i;
            String createdAtStr = "'${hiveconf:" + createdAt + "}'";
            parameters.put(createdAt, tweet.getCreated_at());

            String country = "country" + i;
            String countryStr = "'${hiveconf:" + country + "}'";
            parameters.put(country, tweet.getCountry());

            String retweetCount = "retweet_count" + i;
            String retweetCountStr = "'${hiveconf:" + retweetCount + "}'";
            parameters.put(retweetCount, tweet.getRetweetCount());

            String favoriteCount = "favorite_count" + i;
            String favoriteCountStr = "'${hiveconf:" + favoriteCount + "}'";
            parameters.put(favoriteCount, tweet.getFavouriteCount());

            String lang = "lang" + i;
            String langStr = "'${hiveconf:" + lang + "}'";
            parameters.put(lang, tweet.getLang());

            String fallowersCount = "followers_count" + i;
            String fallowersCountStr = "'${hiveconf:" + fallowersCount + "}'";
            parameters.put(fallowersCount, tweet.getUserFollowersCount());

            String text = "text" + i;
            String textStr = "'${hiveconf:" + text + "}'";
            parameters.put(text, tweet.getText().replace("'", "''"));

            String hashtags = "hashtags" + i;
            String hashtagsStr = "'${hiveconf:" + hashtags + "}'";
            List<String> hashtagsList = tweet.getHashtags();
            hashtagsList =
                hashtagsList.stream().map(s -> s = "" + s + "").collect(Collectors.toList());

            String hashtagsValue = StringUtils.join(hashtagsList, " ");
            parameters.put(hashtags, hashtagsValue);

            query +=
                "" + idStr + "," + createdAtStr + "," + countryStr + "," + retweetCountStr + ","
                    + favoriteCountStr + "," + langStr + "," + fallowersCountStr + "," + textStr
                    + "," + hashtagsStr;
            if (i < tweets.size() - 1) {
                query += "), ";
            }
        }
        query += ")";
        System.out.println(query.toString());
        System.out.println(parameters.toString());
        //hiveOperations.query("create table if not exists  dummy (dummy string)");
        //hiveOperations.query("insert into table dummy values ('asddfg')");

        /*hiveOperations.query("drop table dummy");
        hiveOperations.query(
            "create external table if not exists dummy (dummy string)");
        hiveOperations.query("select * from dummy");
        hiveOperations
            .query("load data local inpath '/home/centos/bigdata/hive-files' into table dummy");*/
        hiveOperations.query(query, parameters);
    }
}
