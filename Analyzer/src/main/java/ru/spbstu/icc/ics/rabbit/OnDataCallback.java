package ru.spbstu.icc.ics.rabbit;

/**
 * Created by Nina Lukashina on 16.12.2015.
 */
public interface OnDataCallback {
    void saveData(String message);
}
