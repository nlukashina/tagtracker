package ru.spbstu.icc.ics.model.hive;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Model of Tweet for analyzer.
 * Created by Nina Lukashina on 12.12.2015.
 */
public class Tweet {
    private String id;
    private LocalDateTime created_at;
    private String country;
    private Integer retweetCount;
    private Integer favouriteCount;
    private String lang;
    private Integer userFollowersCount;
    private String text;
    private List<String> hashtags;

    public Tweet() {
    }

    public Tweet(String id, LocalDateTime created_at, String country, Integer retweetCount,
        Integer favouriteCount, String lang, Integer userFollowersCount, String text,
        List<String> hashtags) {
        this.id = id;
        this.created_at = created_at;
        this.country = country;
        this.retweetCount = retweetCount;
        this.favouriteCount = favouriteCount;
        this.lang = lang;
        this.userFollowersCount = userFollowersCount;
        this.text = text;
        this.hashtags = hashtags;
    }

    public List<String> getHashtags() {
        return hashtags;
    }

    public void setHashtags(List<String> hashtags) {
        this.hashtags = hashtags;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getRetweetCount() {
        return retweetCount;
    }

    public void setRetweetCount(Integer retweetCount) {
        this.retweetCount = retweetCount;
    }

    public Integer getFavouriteCount() {
        return favouriteCount;
    }

    public void setFavouriteCount(Integer favouriteCount) {
        this.favouriteCount = favouriteCount;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Integer getUserFollowersCount() {
        return userFollowersCount;
    }

    public void setUserFollowersCount(Integer userFollowersCount) {
        this.userFollowersCount = userFollowersCount;
    }
}
