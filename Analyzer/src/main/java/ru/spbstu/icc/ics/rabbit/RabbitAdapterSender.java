package ru.spbstu.icc.ics.rabbit;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

/**
 * Sender.
 * <p/>
 * Created by Nina Lukashina on 16.12.2015.
 */
public class RabbitAdapterSender {

    private static final String EXCHANGE_NAME_SEND = "upload-data-message-exchange";
    private static final String ROUTING_KEY_SEND = "upload-data-routing_key";
    Channel channelSend;

    public RabbitAdapterSender() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUri("amqp://195.208.117.141");
        } catch (URISyntaxException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        Connection connection = factory.newConnection();
        channelSend = connection.createChannel();
    }

    public void sendMessage(String msg) throws IOException {
        channelSend.basicPublish(EXCHANGE_NAME_SEND, ROUTING_KEY_SEND, null, msg.getBytes());
        System.out.println("Send " + msg);
    }

}
