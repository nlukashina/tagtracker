'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _mongodb = require('mongodb');

var _mongodb2 = _interopRequireDefault(_mongodb);

var _amqplib = require('amqplib');

var _amqplib2 = _interopRequireDefault(_amqplib);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _fibers = require('fibers');

var _fibers2 = _interopRequireDefault(_fibers);

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

var _config = require('./config');

var DataObtain = (function () {
	function DataObtain() {
		_classCallCheck(this, DataObtain);

		this.MongoClient = _mongodb2['default'].MongoClient;
		this.skip = 0;
	}

	_createClass(DataObtain, [{
		key: 'connectToMongo',
		value: function connectToMongo() {
			var _this = this;

			this.MongoClient.connect('mongodb://' + _config.mongo.ip + ':' + _config.mongo.port + '/' + _config.mongo.db, function (err, db) {
				_assert2['default'].equal(null, err);
				console.log("Successfully connected to the database");
				_this.db = db;
			});
		}
	}, {
		key: 'connectToRabbit',
		value: function connectToRabbit() {
			var _this2 = this;

			var connect = _amqplib2['default'].connect('amqp://' + _config.rabbit.ip + ':' + _config.rabbit.port);
			connect.then(function (conn) {
				_this2.rabbit = conn;
				_this2.setRabbitListener();

				// Tests
				// this.rabbitGetMsgTest();
				// this.rabbitGetStoredData();
			});
		}
	}, {
		key: 'setRabbitListener',
		value: function setRabbitListener() {
			var _this3 = this;

			var conn = this.rabbit;
			conn.createChannel().then(function (ch) {
				var ex = _config.rabbit.receive.exchange;
				var q = _config.rabbit.receive.queue;
				var key = _config.rabbit.receive.key;

				var ok = ch.assertExchange(ex, 'fanout', { durable: false });

				ok = ok.then(function () {
					return ch.assertQueue(q, { durable: false });
				});

				ok = ok.then(function () {
					return ch.bindQueue(q, ex, key);
				});

				return ok.then(function () {
					var callabck = function callabck() {
						var newCh = ch;
						var queue = q;
						newCh.consume(queue, function (msg) {
							console.log('Get message: ', msg.content.toString());
							if (msg.content.toString() === _config.rabbit.receive.message) {
								_this3.sendTweets();
							}
						}, { noAck: true });
					};
					setInterval(callabck, 500);
				});
			}).then(null, console.warn);
		}
	}, {
		key: 'sendTweets',
		value: function sendTweets() {
			var _this4 = this;

			var conn = this.rabbit;
			conn.createChannel().then(function (ch) {
				var ex = _config.rabbit.send.exchange;
				var key = _config.rabbit.send.key;

				var ok = ch.assertExchange(ex, 'direct', { durable: false });
				ok = ok.then(function () {
					(0, _fibers2['default'])(function () {
						while (_this4.skip !== -1) {
							_this4.setTweets();
							if (_this4.tweets.length != 0) {
								var twt = { type: 1, tweets: _this4.tweets };
								var _msg = new Buffer(JSON.stringify(twt));
								ch.publish(ex, key, _msg);
							}
						}
						_this4.skip = 0;
						var msg = new Buffer(JSON.stringify({ type: 2 }));
						ch.publish(ex, key, msg);
					}).run();
				});
			}).then(null, console.warn);
		}
	}, {
		key: 'setTweets',
		value: function setTweets() {
			var _this5 = this;

			var tweets = [];
			var cursor = this.db.collection(_config.mongo.collection).find({}, { limit: _config.rabbit.msg_count, skip: this.skip });

			var fiber = _fibers2['default'].current;
			cursor.toArray(function (err, res) {
				_assert2['default'].equal(err, null);
				_this5.tweets = res.map(_this5.formatTweet);
				_this5.skip = res.length === _config.rabbit.msg_count ? _this5.skip + _config.rabbit.msg_count : -1;
				fiber.run();
			});
			_fibers2['default']['yield']();
		}
	}, {
		key: 'formatTweet',
		value: function formatTweet(tweet) {
			var twt = {
				id: tweet.id,
				text: tweet.text,
				lang: tweet.lang,
				createdAt: tweet.createdAt,
				hashtags: tweet.etities.hashtags,
				country: tweet.place.country,
				followers_count: tweet.user.followers_count,
				retweet_count: tweet.retweet_count,
				favorite_count: tweet.favorite_count
			};

			return twt;
		}
	}, {
		key: 'start',
		value: function start() {
			_q2['default'].fcall(this.connectToMongo.bind(this)).then(this.connectToRabbit.bind(this))['catch'](function (err) {
				throw new Error(err);
			}).done();
		}

		// Tests
	}, {
		key: 'rabbitGetMsgTest',
		value: function rabbitGetMsgTest() {
			var conn = this.rabbit;
			conn.createChannel().then(function (ch) {
				var msg = _config.rabbit.receive.message;
				var ex = _config.rabbit.receive.exchange;
				var key = _config.rabbit.receive.key;

				var ok = ch.assertExchange(ex, 'fanout', { durable: false });

				return ok.then(function () {
					var callabck = function callabck() {
						console.log('Publish: ', new Buffer(msg));
						ch.publish(ex, key, new Buffer(msg));
					};
					callabck();
					//setInterval(callabck, 2000);
				});
			}).then(null, console.warn);
		}
	}, {
		key: 'rabbitGetStoredData',
		value: function rabbitGetStoredData() {
			var conn = this.rabbit;
			conn.createChannel().then(function (ch) {
				var ex = _config.rabbit.send.exchange;
				var q = 'send_queue';
				var key = _config.rabbit.send.key;

				var ok = ch.assertExchange(ex, 'fanout', { durable: false });

				ok = ok.then(function () {
					return ch.assertQueue(q, { durable: false });
				});

				ok = ok.then(function () {
					return ch.bindQueue(q, ex, key);
				});

				return ok.then(function () {
					var callabck = function callabck() {
						var newCh = ch;
						var queue = q;
						newCh.consume(queue, function (msg) {
							console.log('Get stored data: ', msg.content.toString());
						}, { noAck: true });
					};
					setInterval(callabck, 500);
				});
			}).then(null, console.warn);
		}
	}]);

	return DataObtain;
})();

new DataObtain().start();