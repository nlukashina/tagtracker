angular.module('ui.bigData', ['ngAnimate', 'ui.bootstrap']);
angular.module('ui.bigData').controller('TypeaheadCtrl', function($scope, $http) {

  $scope.selected = undefined;
  //$scope.tags = ['#Exam','#Example','#ExampleTags'];
  
  

  var req = new XMLHttpRequest();
  $scope.getTags = function(val) {
		
    return $http.get('localhost:8080/tag/autocomplete', { 
      params: {
        teg: val
      }
    }).then(function(response){
      return response.JSON.parse(req.responseText);
    });
  };

 
});