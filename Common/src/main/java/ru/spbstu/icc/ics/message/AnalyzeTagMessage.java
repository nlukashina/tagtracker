package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;

/**
 * Created by Nina Lukashina on 04.11.2015.
 */
public class AnalyzeTagMessage extends TagTrackerMessage {

    // Tag to analyze.
    private String tag;

    public AnalyzeTagMessage() {
        this.messageType = MessageType.ANALYZE_TAG;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
