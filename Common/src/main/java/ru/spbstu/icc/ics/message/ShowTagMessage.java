package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;

import java.util.List;

/**
 * Created by Nina Lukashina on 04.11.2015.
 */
public class ShowTagMessage extends TagTrackerMessage {
    // Tags to show on UI.
    private List<String> tags;

    public ShowTagMessage() {
        this.messageType = MessageType.SHOW_TAG;
    }
}
