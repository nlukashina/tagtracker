package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;

/**
 * Abstract class which represents a message in the app.
 * <p>
 * Created by Nina Lukashina on 06.10.2015.
 */
public class TagTrackerMessage{
    /**
     * Type of message. Used in consumers to indicate what is it.
     */
    protected MessageType messageType;
    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }
}
