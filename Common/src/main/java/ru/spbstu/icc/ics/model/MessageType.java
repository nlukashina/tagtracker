package ru.spbstu.icc.ics.model;

/**
 * Created by Nina Lukashina on 06.10.2015.
 */
public enum MessageType {
    UPLOAD_DATA(1L, "Upload data"),
    PROCESS_UPLOADED_DATA(2L, "Process uploaded data"),
    ANALYZE_TAG(3L, "Analyze tag"),
    VISUALIZE_DATA(4L, "Visualize tag"),
    SEARCH_TAG(5L, "Search tag"),
    SHOW_TAG(6L, "Search tag"),
    COUNT_TAG(7L, "Count tweets with specified tag"),
    SHOW_COUNT_TAG(8L, "Count tweets with specified tag"),
    ANALYZE_IN_PROGRESS(9L, "Info about anilize");
    
    private String description;
    private Long id;

    MessageType(Long id, String description) {
        this.description = description;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
