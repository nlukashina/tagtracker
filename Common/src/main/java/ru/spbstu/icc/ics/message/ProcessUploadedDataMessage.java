package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;
import ru.spbstu.icc.ics.model.Tweet;

import java.util.List;

/**
 * This message, which is sent from Uploader to Storage, Analyzer and Searcher, when data is uploaded.
 * When receive this message, module must process uploaded data from message according to its
 * <p>
 * Created by Nina Lukashina on 05.10.2015.
 */
public class ProcessUploadedDataMessage extends TagTrackerMessage {
    //TODO: review

    /**
     * List of uploaded tweets from Twitter.
     */
    private List<Tweet> data;

    public ProcessUploadedDataMessage(List<Tweet> data) {
        this.data = data;
        this.messageType = MessageType.PROCESS_UPLOADED_DATA;
    }

    public List<Tweet> getData() {
        return data;
    }

    public void setData(List<Tweet> data) {
        this.data = data;
    }
}
