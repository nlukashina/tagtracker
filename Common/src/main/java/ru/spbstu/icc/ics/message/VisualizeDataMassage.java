package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;

import java.util.Map;

/**
 * Created by Nina Lukashina on 04.11.2015.
 */
public class VisualizeDataMassage extends TagTrackerMessage {

    /**
     * Data is a map of number of tweets to time period.
     * Keys: 1 - 6 a.m. - 12 a.m
     * 2 - 12 a.m. - 18 p.m
     * 3 - 18 a.m. - 12 p.m
     * 4 - 12 p.m. - 6 a.m
     *
     * Example: (2, 3) = 3 tweets from 12 a.m. to 18 p.m
     */
    private Map<Integer, Integer> data;

    public VisualizeDataMassage() {
        this.messageType = MessageType.VISUALIZE_DATA;
    }
}
