package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;

/**
 * Created by Nina Lukashina on 04.11.2015.
 */
public class SearchTagMessage extends TagTrackerMessage  {

    // Tag to search.
    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public SearchTagMessage() {
        this.messageType = MessageType.SEARCH_TAG;
    }
}
