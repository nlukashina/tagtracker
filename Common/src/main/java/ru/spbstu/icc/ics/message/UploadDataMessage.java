package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;

/**
 * This message is sent from Server to Uploader,when new data should be uploaded from Twitter.
 * Created by Nina Lukashina on 06.10.2015.
 */
public class UploadDataMessage extends TagTrackerMessage {
    //TODO: review

    UploadDataMessage() {
        this.messageType = MessageType.UPLOAD_DATA;
    }
}
