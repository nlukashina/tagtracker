package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;

public class AnalyzeInProgressMessage extends TagTrackerMessage {

	public AnalyzeInProgressMessage() {
        this.messageType = MessageType.ANALYZE_IN_PROGRESS;
	}
}
