package ru.spbstu.icc.ics.message;

import ru.spbstu.icc.ics.model.MessageType;

/**
 * Created by Anton Kashcheev on 26.11.2015.
 */
public class ShowCountTagMessage extends TagTrackerMessage {
	// Count searched tag.
	private Integer count;
	
	public Integer getCount() {
		return count;
	}
	
	public void setCount(Integer count) {
		this.count = count;
	}
	
	public ShowCountTagMessage() {
		this.messageType = MessageType.SHOW_COUNT_TAG;
	}
}
