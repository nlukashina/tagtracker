package ru.spbstu.icc.ics;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import org.bson.Document;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class DataStorage {
    static MongoClient mongoClient;

    public static void main(String[] args) {
        RabbitAdapterReceiver receiver = null;
        RabbitAdapterSender sender = null;
        try {
            receiver = new RabbitAdapterReceiver("localhost", callback);
            sender = new RabbitAdapterSender("localhost");
            mongoClient = new MongoClient("195.208.117.141", 27017);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        try {
            sender.sendMessage("{type:1}");
            receiver.receiveMessages();
        } catch (IOException e) {
        }
    }

    static OnDataCallback callback = new OnDataCallback() {

        public void saveData(String message) {
            System.out.println(message);
            if (message.equals("{type: 2}"))
                return;
            DBObject doc = new BasicDBObject();
            doc = (DBObject) JSON.parse(message);
            MongoDatabase dataStorage = mongoClient.getDatabase("DataStorage");
            MongoCollection<Document> data = dataStorage.getCollection("Data");
            Document document = Document.parse(message);
            data.insertOne(document);
            /*DB db = mongoClient.getDB("DataStorage");
            DBCollection collection = db.getCollection("Data");
            collection.insert(doc);*/
        }

    };

}
