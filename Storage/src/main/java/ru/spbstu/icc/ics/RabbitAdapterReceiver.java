package ru.spbstu.icc.ics;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

interface OnDataCallback {
    public void saveData(String message);
}


public class RabbitAdapterReceiver {
    private static final String EXCHANGE_NAME_RECEIVE = "process-uploaded-data-message-exchange";
    private static final String ROUTING_KEY_RECEIVE = "process-uploaded-data-routing_key";
    Connection connection;
    Channel channelReceive;
    Consumer consumer;
    String queueName;
    OnDataCallback callback;

    public RabbitAdapterReceiver(String host, OnDataCallback c)
        throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        //factory.setHost(host);
        try {
            factory.setUri("amqp://195.208.117.141");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        Connection connection = factory.newConnection();
        channelReceive = connection.createChannel();
        callback = c;
        channelReceive.exchangeDeclare(EXCHANGE_NAME_RECEIVE, "direct");
        queueName = channelReceive.queueDeclare().getQueue();
        channelReceive.queueBind(queueName, EXCHANGE_NAME_RECEIVE, ROUTING_KEY_RECEIVE);

        consumer = new DefaultConsumer(channelReceive) {
            @Override public void handleDelivery(String consumerTag, Envelope envelope,
                AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body);
                System.out.println("Received new message.");
                callback.saveData(message);
            }
        };
        //channel.basicConsume(queueName, true, consumer);
    }

    public void receiveMessages() throws IOException {

        channelReceive.basicConsume(queueName, true, consumer);
        System.out.println("Start receiveing");
    }

}
