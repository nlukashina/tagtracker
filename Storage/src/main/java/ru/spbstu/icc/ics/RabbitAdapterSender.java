package ru.spbstu.icc.ics;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class RabbitAdapterSender {
    private static final String EXCHANGE_NAME_SEND = "upload-data-message-exchange";
    private static final String ROUTING_KEY_SEND = "upload-data-routing_key";
    Connection connection;
    Channel channelSend;
    String queueName;

    public RabbitAdapterSender(String host) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUri("amqp://195.208.117.141");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        //factory.setHost(host);
        Connection connection = factory.newConnection();
        channelSend = connection.createChannel();
        //channelSend.exchangeDeclare(EXCHANGE_NAME_SEND, "direct");
    }

    public void sendMessage(String msg) throws IOException {
        channelSend.basicPublish(EXCHANGE_NAME_SEND, ROUTING_KEY_SEND, null, msg.getBytes());
        System.out.println("Send " + msg);
    }

}
