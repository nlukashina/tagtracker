import mongodb from 'mongodb';
import rabbitmq from 'amqplib';
import assert from 'assert';
import Fiber from 'fibers';
import Q from 'q';
import {mongo, rabbit} from './config'
 

class DataObtain {
	constructor() {
		this.MongoClient = mongodb.MongoClient;
		this.skip = 0;
	}

	connectToMongo() {
		this.MongoClient.connect(`mongodb://${mongo.ip}:${mongo.port}/${mongo.db}`, (err, db) => {
			assert.equal(null, err);
	        console.log("Successfully connected to the database");
	        this.db = db;
		});
	}

	connectToRabbit() {
		var connect = rabbitmq.connect(`amqp://${rabbit.ip}:${rabbit.port}`);
		connect.then((conn) => {
			this.rabbit = conn;
			this.setRabbitListener();

			// Tests
			// this.rabbitGetMsgTest();
			// this.rabbitGetStoredData();
		});
	}

	setRabbitListener() {
		const conn = this.rabbit;
		conn.createChannel().then((ch) => {
			const ex = rabbit.receive.exchange;
			const q = rabbit.receive.queue;
			const key = rabbit.receive.key;

			var ok = ch.assertExchange(ex, 'fanout', {durable: false});

			ok = ok.then(() => {
				return ch.assertQueue(q, {durable: false});
			});

			ok = ok.then(() => {
				return ch.bindQueue(q, ex, key);
			});

			return ok.then(() => {
				const callabck = () => {
					const newCh = ch;
					const queue = q;
					newCh.consume(queue, (msg) => {
						console.log('Get message: ', msg.content.toString());
						if (msg.content.toString() === rabbit.receive.message) {
							this.sendTweets();
						}
					}, {noAck: true});
				};
				setInterval(callabck, 500);
			});
		}).then(null, console.warn);
	}

	sendTweets() {
		const conn = this.rabbit;
		conn.createChannel()
		.then((ch) => {
			const ex = rabbit.send.exchange;
			const key = rabbit.send.key;

			var ok = ch.assertExchange(ex, 'direct', {durable: false});
			ok = ok.then(() => {
				Fiber(() => {
					while (this.skip !== -1) {
						this.setTweets();
						if (this.tweets.length != 0) {
							const twt = {type: 1, tweets: this.tweets};
							const msg = new Buffer(JSON.stringify(twt));
							ch.publish(ex, key, msg);
						}
					}
					this.skip = 0;
					var msg = new Buffer(JSON.stringify({type: 2}));
					ch.publish(ex, key, msg);
				}).run();
			});
		}).then(null, console.warn);
	}

	setTweets() {
		var tweets = [];
		var cursor = this.db.collection(mongo.collection).find({}, {limit: rabbit.msg_count, skip: this.skip});

		var fiber = Fiber.current;
		cursor.toArray((err, res) => {
			assert.equal(err, null);
			this.tweets = res.map(this.formatTweet);
			this.skip = (res.length === rabbit.msg_count) ? (this.skip + rabbit.msg_count) : -1;
			fiber.run();
		});
		Fiber.yield();
	}

	formatTweet(tweet) {
		const twt = {
			id: tweet.id,
			text: tweet.text,
			lang: tweet.lang,
			createdAt: tweet.createdAt,
			hashtags: tweet.etities.hashtags,
			country: tweet.place.country,
			followers_count: tweet.user.followers_count,
			retweet_count: tweet.retweet_count,
			favorite_count: tweet.favorite_count,
		};

		return twt;
	}

	start() {
		Q.fcall(this.connectToMongo.bind(this))
		.then(this.connectToRabbit.bind(this))
		.catch((err) => {
		    throw new Error(err);
		})
		.done();
	}

	// Tests
	rabbitGetMsgTest() {
		const conn = this.rabbit;
		conn.createChannel().then((ch) => {
			const msg = rabbit.receive.message;
			const ex = rabbit.receive.exchange;
			const key = rabbit.receive.key;

			var ok = ch.assertExchange(ex, 'fanout', {durable: false});

			return ok.then(() => {
				const callabck = () => {
					console.log('Publish: ', new Buffer(msg))
					ch.publish(ex, key, new Buffer(msg));
				};
				callabck();
				//setInterval(callabck, 2000);
			});
		}).then(null, console.warn);
	}

	rabbitGetStoredData() {
		const conn = this.rabbit;
		conn.createChannel().then((ch) => {
			const ex = rabbit.send.exchange;
			const q = 'send_queue';
			const key = rabbit.send.key;

			var ok = ch.assertExchange(ex, 'fanout', {durable: false});

			ok = ok.then(() => {
				return ch.assertQueue(q, {durable: false});
			});

			ok = ok.then(() => {
				return ch.bindQueue(q, ex, key);
			});

			return ok.then(() => {
				const callabck = () => {
					const newCh = ch;
					const queue = q;
					newCh.consume(queue, (msg) => {
						console.log('Get stored data: ', msg.content.toString());
					}, {noAck: true});
				};
				setInterval(callabck, 500);
			});
		}).then(null, console.warn);
	}
}

(new DataObtain()).start();
