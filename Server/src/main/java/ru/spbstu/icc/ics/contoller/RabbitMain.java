package ru.spbstu.icc.ics.contoller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by localhost on 15.11.2015.
 */

@SpringBootApplication
public class RabbitMain {
    public static void main(String[] args) {
        SpringApplication.run(RabbitMain.class, args);
    }
}
