package ru.spbstu.icc.ics.contoller;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by localhost on 16.12.2015.
 */
public class ResponceHashMap {
    private static ResponceHashMap object;
    private Map<String, String> responseMap;

    private ResponceHashMap() {
        responseMap = new HashMap<String, String>();
    }

    public static synchronized ResponceHashMap instance() {
        if (object == null) {
            object = new ResponceHashMap();
        }

        return object;
    }

    public void clearMap() {
        responseMap.clear();
    }

    public void setNewResponse(String key, String response) {
        if (responseMap.containsKey(key)) {
            responseMap.remove(key);
        }
        responseMap.put(key, response);
    }

    public String getResponceForKey(String key) {
        if (responseMap.containsKey(key)) {
            return responseMap.get(key);
        }

        return "";
    }

    public void removeResponceForKey(String key) {
        responseMap.remove(key);
    }
}
