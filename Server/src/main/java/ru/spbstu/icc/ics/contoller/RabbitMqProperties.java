package ru.spbstu.icc.ics.contoller;

public final class RabbitMqProperties {
    public static final String uploadedReceiveQueueName = "uploader-receive-queue";
    public static final String searcherReceiveQueueName = "searcher-receive-queue";
    public static final String analyzerReceiveQueueName = "analyzer-receive-queue";
    public static final String storageReceiveQueueName = "storage-receive-queue";
    public static final String visualizerReceiveQueueName = "visualizer-receive-queue";
    public static final String serverReceiveQueueName = "server-receive-queue";
    public static final String analyzeProgressQueueName = "analyze-progress-queue";

    public static final String uploadDataMessageExchangeName = "upload-data-message-exchange";
    public static final String processUploadedDataMessageExchangeName =
            "process-uploaded-data-message-exchange";
    public static final String analyzeTagMessageExchangeName = "analyze-tag-message-exchange";
    public static final String visualizeDataMessageExchangeName = "visualize-data-message-exchange";
    public static final String searchTagMessageExchangeName = "search-tag-message-exchange";
    public static final String showTagMessageExchangeName = "show-tag-message-exchange";
    public static final String analyzeProgressExchangeName = "analyze-progress-exchange";
    public static final String analyzeDoneExchangeName = "analyze-done-exchange";

    public static final String processUploadedDataRoutingKey = "process-uploaded-data-routing_key";
    public static final String uploadDataRoutingKey = "upload-data-routing_key";
    public static final String analyzeTagRoutingKey = "analyze-tag-routing-key";
    public static final String visualizeDataRoutingKey = "visualize-data-routing-key";
    public static final String searchTagRoutingKey = "search-tag-routing-key";
    public static final String showTagRoutingKey = "show-tag-routing-key";
    public static final String analyzeProgressRoutingKey = "analyze-progress-routing-key";
    public static final String analyzeDoneRoutingKey = "analyze-done-routing-key";
}
