package ru.spbstu.icc.ics.contoller;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

/**
 * Created by localhost on 27.11.2015.
 */
public class RabbitSenderr {

    private String queueName;
    private String exchangeName;
    private String routingKey;

    public RabbitSenderr(String queueName, String exchangeName, String routingKey) {
        this.queueName = queueName;
        this.exchangeName = exchangeName;
        this.routingKey = routingKey;
    }

    public void sendMessageToRabbit(long type, String tag) {
        try {
            JSONObject message = new JSONObject();
            message.put("tag", tag);
            message.put("type", type);
            System.out.println(" [x] Sent '" + message + "'");
            sendMessage(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendMessageToRabbit(long type, String tag, int analyze_type) {
        try {
            JSONObject message = new JSONObject();
            message.put("tag", tag);
            message.put("type", type);
            message.put("analyze_type", analyze_type);
            System.out.println(" [x] Sent '" + message + "'");
            sendMessage(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendMessage(JSONObject message) throws Exception {
        boolean key = false;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://195.208.117.141");
        Connection conn = null;
        while(!key) {
            try {
                conn = factory.newConnection();
                key = true;
            } catch (Exception ex) {

            }
        }
        Channel channel = conn.createChannel();

        channel.exchangeDeclare(exchangeName, "direct");
        channel.queueBind(queueName, exchangeName, routingKey);

        channel.basicPublish(exchangeName, routingKey, null, message.toString().getBytes());

        channel.close();
        conn.close();
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public String getQueueName() {
        return queueName;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public String getRoutingKey() {
        return routingKey;
    }
}
