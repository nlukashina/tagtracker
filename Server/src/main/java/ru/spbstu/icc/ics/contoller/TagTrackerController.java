package ru.spbstu.icc.ics.contoller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;

@RestController
public class TagTrackerController {

    @RequestMapping(value = "/tag/autocomplete", method = RequestMethod.GET)
    public ResponseEntity<String> autocompleteGET() {
        String response = ResponceHashMap.instance().getResponceForKey("autocomplete");
        return generateResponse(response);
    }

    @RequestMapping(value = "/tag/autocomplete/notpost", method = RequestMethod.GET)
    public ResponseEntity<String> autocompletePost(String tag) {
        // TODO: implement
        // Message to send: SearchTagMessage

        // Send to (rabbitMq credentials): ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.searcherReceiveQueueName (queue),
        // ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.searchTagMessageExchangeName (exchange),
        // ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.searchTagRoutingKey (routing key)

        // Response to UI contains List<String> - tags

        RabbitSenderr sender = new RabbitSenderr(RabbitMqProperties.analyzerReceiveQueueName,
                                                 RabbitMqProperties.analyzeTagMessageExchangeName,
                                                 RabbitMqProperties.analyzeTagRoutingKey);

        RabbitReceiver receiver = new RabbitReceiver(RabbitMqProperties.serverReceiveQueueName,
                                                     RabbitMqProperties.showTagMessageExchangeName,
                                                     RabbitMqProperties.showTagRoutingKey);
        try {
            sender.sendMessageToRabbit(5L, tag);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
           receiver.receiveMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return generateNotPostResonse();
    }

    @RequestMapping(value = "/tag/analyze", method = RequestMethod.GET)
    public ResponseEntity<String> analyzeGet() {
        String response = ResponceHashMap.instance().getResponceForKey("analyze");
        ResponseEntity<String> res = generateResponse(response);

        //ResponceHashMap.instance().removeResponceForKey("analyze");

        return res;
    }

    @RequestMapping(value = "/tag/analyze/notpost", method = RequestMethod.GET)
    public ResponseEntity<String> analyzePost(String tag, int analyze_type) {
        // TODO: implement

        // Message to send: AnalyzeTagMessage

        // Send to (rabbitMq credentials): ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.analyzerReceiveQueueName (queue),
        // ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.analyzeTagMessageExchangeName (exchange),
        // ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.analyzeTagRoutingKey (routing key)

        // Response to UI contains File. See: http://www.mkyong.com/webservices/jax-rs/download-image-file-from-jax-rs/

        RabbitSenderr sender = new RabbitSenderr(RabbitMqProperties.analyzerReceiveQueueName,
                RabbitMqProperties.analyzeTagMessageExchangeName,
                RabbitMqProperties.analyzeTagRoutingKey);

        RabbitReceiver receiver = new RabbitReceiver(RabbitMqProperties.analyzeProgressQueueName,
                RabbitMqProperties.analyzeProgressExchangeName,
                RabbitMqProperties.analyzeProgressRoutingKey);
        try {
            sender.sendMessageToRabbit(3L, tag, analyze_type);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            receiver.receiveMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return generateNotPostResonse();
    }

    @RequestMapping(value = "/tag/count", method = RequestMethod.GET)
    public ResponseEntity<String> countGET() {
        String response = ResponceHashMap.instance().getResponceForKey("count");
        return generateResponse(response);
    }

    @RequestMapping(value = "/tag/count/notpost", method = RequestMethod.GET)
    public ResponseEntity<String> countPOST(String tag) {
        // TODO: implement

        // Message to send: CountTagMessage

        // Send to (rabbitMq credentials): ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.searcherReceiveQueueName (queue),
        // ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.searchTagMessageExchangeName (exchange),
        // ru.spbstu.icc.ics.model.contollers.RabbitMqProperties.searchTagRoutingKey (routing key)
        // ??? May be changed in future

        // Response to UI contains Integer (number of tags

        RabbitSenderr sender = new RabbitSenderr(RabbitMqProperties.analyzerReceiveQueueName,
                RabbitMqProperties.analyzeTagMessageExchangeName,
                RabbitMqProperties.analyzeTagRoutingKey);

        RabbitReceiver receiver = new RabbitReceiver(RabbitMqProperties.serverReceiveQueueName,
                RabbitMqProperties.showTagMessageExchangeName,
                RabbitMqProperties.showTagRoutingKey);
        try {
            sender.sendMessageToRabbit(7L, tag);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            receiver.receiveMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return generateNotPostResonse();
    }

    private ResponseEntity<String> generateResponse(String response) {
        JSONObject result = new JSONObject();

        try {
            if (response.length() > 0) {
                result.put("status", "success");
            } else {
                result.put("status", "waiting");
            }
            result.put("response", response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Access-Control-Allow-Origin", "*");

        return new ResponseEntity<String>(result.toString(), headers, HttpStatus.OK);
    }

    private ResponseEntity<String> generateNotPostResonse() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("status", "OK");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Access-Control-Allow-Origin", "*");

        return new ResponseEntity<String>(obj.toString(), headers, HttpStatus.OK);
    }

}
