package ru.spbstu.icc.ics.contoller;

import com.rabbitmq.client.*;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

/**
 * Created by localhost on 22.11.2015.
 */
public class RabbitReceiver {
    private String queueName;
    private String exchangeName;
    private String routingKey;

    public RabbitReceiver(String queueName, String exchangeName, String routingKey) {
        this.queueName = queueName;
        this.exchangeName = exchangeName;
        this.routingKey = routingKey;
    }

    public void receiveMessage() throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, TimeoutException {
        Channel channelReceive;
        Consumer consumer;
        boolean key = false;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://195.208.117.141");
        Connection connection = null;
        while(!key) {
            try {
                connection = factory.newConnection();
                key = true;
            } catch (Exception ex) {

            }
        }
        channelReceive = connection.createChannel();
        channelReceive.exchangeDeclare(exchangeName, "direct");
        channelReceive.queueBind(queueName, exchangeName, routingKey);

        consumer = new DefaultConsumer(channelReceive) {
            @Override public void handleDelivery(String consumerTag, Envelope envelope,
                                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body);
                System.out.println("Received new message: " + message);
                message = message.replaceAll("\\n", "");
                message = message.replaceAll("\\t", "");
                try {
                    JSONObject object = new JSONObject(message);
                    int type = object.optInt("type");
                    switch (type) {
                        case 6: {
                            System.out.println("Autocomplete to map");
                            ResponceHashMap.instance().setNewResponse("autocomplete", message);
                            break;
                        }
                        case 8: {
                            System.out.println("Count to map");
                            ResponceHashMap.instance().setNewResponse("count", message);
                            break;
                        }
                        case 9: {
                            System.out.println("Start graph receiver");
                            ResponceHashMap.instance().removeResponceForKey("analyze");
                            startGraphReceiver();
                            break;
                        }
                        case 10: {
                            System.out.println("Graph to map");
                            parseGraphResponse(message);
                            break;
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        channelReceive.basicConsume(queueName, true, consumer);
        System.out.println("Start receiveing");
    }

    private void startGraphReceiver() {
        RabbitReceiver receiver = new RabbitReceiver(RabbitMqProperties.serverReceiveQueueName,
                RabbitMqProperties.analyzeDoneExchangeName,
                RabbitMqProperties.analyzeDoneRoutingKey);

        try {
            receiver.receiveMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseGraphResponse(String message) {
        try {
            JSONObject obj = new JSONObject(message);

            String status = obj.optString("status");
            if(status.equals("success")) {
                String url = obj.optString("url");
                ResponceHashMap.instance().setNewResponse("analyze", url);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public String getQueueName() {
        return queueName;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public String getRoutingKey() {
        return routingKey;
    }
}
