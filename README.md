/####  
HOWTO: «Запуск модуля»  
/####  

1. Установить NodeJS (v0.12.7) - https://nodejs.org/en/download/releases/
2. В папке с модулем выполнить команду (через терминал) npm install
3. В config.js в константах mongo и rabbit указать нужные параметры.
4. Запустить модуль выполнив команду node ./dist/uploader.js в корне модуля.
  
// P.S. MongoDB и RabbitMQ должны быть запущены до запуска модуля
  
/####  
Разработчику модуля хранения.  
/####  

Данные о твиттах прилетают в rabbitmq:  
  * exchange: 'process-uploaded-data-message-exchange'  
  * routingKey: 'process-uploaded-data-routing_key'  

Прилетают порциями по 50 твиттов(можно менять в config.js) в формате JSON в виде объекта из типа сообщения и массива твиттов: 
```
{  
  type: 2,  
  tweets: [  
      {  
          id: "",  
          text: "",  
          lang: "",
          createdAt: "",  
          hashtags: [],
          country: "",
          followers_count: "",
          retweet_count: "",
          favorite_count: "",
      },  
      …  
  ]  
}  
```
Когда данные закончатся в rabbitmq придет сообщение ‘{type: 2}’. Это означает, что не нужно ждать следующую порцию данных и можно делать что-то дальше.  

Чтобы данные начали прилетать необходимо послать сообщение ‘{type: 1}’ в rabbitmq:  
  * exchange: 'upload-data-message-exchange'  
  * queue: 'uploader-receive-queue'  
  * routingKey: 'upload-data-routing_key'  
