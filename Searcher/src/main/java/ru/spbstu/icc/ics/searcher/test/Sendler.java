package ru.spbstu.icc.ics.searcher.test;

import java.util.LinkedList;

public class Sendler {
	
	static int messageNumber = 0;
	
	static String newJsonMessageType2() {
		String jsonMessage = "";
		jsonMessage =
		"{"
			+ "\n\t" + "\"type\":" + 2 + ","
			+ "\n\t" + "\"tweets\":\n\t[";
			
		for(int i = 0; i < 5; ++i) {
			messageNumber++;
			String jsonTweet =
			"\n\t\t{"
				+ "\n\t\t\t" + "\"text\":" + "\"test-message-" + messageNumber + "\"" + ", "
				+ "\n\t\t\t" + "\"createdAt\":" + "\"" + messageNumber + "-01-15\"" + ", "
				+ "\n\t\t\t" + "\"hashtags\":" + "[" + "\"lolik-" + messageNumber + "\","
													 + "\"ics-stu-2015-" + messageNumber + "\""
													 + "]"
			+ "\n\t\t}";
			
			if(i < 4) {
				jsonMessage += jsonTweet + ",\n\t";
			} else {
				jsonMessage += jsonTweet + "\n";
			}
		}
		
		jsonMessage += "\n\t]\n}";
		
		return jsonMessage;
	}
	
	static String newJsonMessageType2Empty() {
		String jsonMessage =
			"{"
			+ "\n\t\"type\":" + 2
			+ "\n}";
		messageNumber++;
		return jsonMessage;
	}
	
	static String newJsonMessageType5() {
		String jsonMessage =
		"{"
		+ "\n\t" + "\"type\":" + 5 + ','
		+ "\n\t" + "\"hashtags\":" + "[\"lol\"]"
		+ "\n}";
		messageNumber++;
		return jsonMessage;
	}
	
	
	static void sendingType2(RabbitMqTestConnection rabbit) {
		boolean isSended = true;
		for(int i = 0; i < 8; ++i) {
			String message =  "";
			if(i != 4) {
				message = newJsonMessageType2();
			} else {
				message = newJsonMessageType2Empty();
			}
			System.out.println("Message" + i);
			System.out.println(message);
			
			isSended = rabbit.sendMessage(message);
			if(isSended) {
				System.out.println("Sended\n");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					continue;
				}
				
			}
		}
		
		
	}
	
	static void sendingType5(RabbitMqTestConnection rabbit) {
		boolean isSended = true;
		String message =  newJsonMessageType5();
		System.out.println("Message type 5");
		System.out.println(message);
		
		isSended = rabbit.sendMessage(message);
		if(isSended) {
			System.out.println("Sended\n");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			
		}
	}
	
	static void receive(RabbitMqTestConnection rabbit) {
		String receiveMessage;
		receiveMessage = rabbit.getNextMessage();
		if(receiveMessage == null || receiveMessage.isEmpty()) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
		else {
			System.out.println("ReceiveMessage:\n" + receiveMessage);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	public static void runTest() {
		RabbitMqTestConnection rabbitMQConnection
			= new RabbitMqTestConnection("asus-n56v");
		
		System.out.println("Attempting to connect to RabbitMQ...");
		if(rabbitMQConnection.tryConnect()) {
			System.out.println("Successful connection.");
		}
		else {
			System.out.println("Failed connection.");
			return;
		}
		
		sendingType2(rabbitMQConnection);
		
		sendingType5(rabbitMQConnection);
		receive(rabbitMQConnection);
	}
}
