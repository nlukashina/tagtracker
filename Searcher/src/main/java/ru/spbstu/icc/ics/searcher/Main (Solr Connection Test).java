package ru.spbstu.icc.ics.searcher;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.params.ModifiableSolrParams;

import java.io.IOException;
import java.net.URL;

public class Main {
	// That file is for SolrConnection testing.
	public static void main(String[] args) throws IOException, SolrServerException {
		String url = "http://pastebin.com/raw/Qpz13bHd";
		String twJson = IOUtils.toString(new URL(url));
		HttpSolrClient client = new HttpSolrClient("http://localhost:8983/solr/#/tag_tracker");
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(twJson);

		SolrConnection solr = new SolrConnection();

		solr.indexJSON(node);
		ModifiableSolrParams params = new ModifiableSolrParams();

		params.set("q", "tag:ph*");
		System.out.println(solr.searchByQuery(params));
		params.set("q", "tag:first*");
		System.out.println(solr.searchByQuery(params));
		params.set("q", "tag:firste*");
		System.out.println(solr.searchByQuery(params));

		System.out.println(solr.searchByQuery("second"));
		System.out.println(solr.countByQuery("second"));

		System.out.println(solr.countByCountry("*","Norway","Norway2","Deutschland","Sweden"));
		System.out.println(solr.countByLang("*","ru","en","3","4"));
		System.out.println(solr.countByDate("*","Mon","Tue","Wed","Thu","Fri","Sat","Sun"));
		System.out.println(solr.countByRT("*",0,200,200,400,400,600,600,800));
		System.out.println(solr.countByFollowers("*",0,200,200,400,400,600,600,800));
		System.out.println(solr.countByFavourite("*",0,200,200,400,400,600,600,800));

		System.out.println(solr.deleteByQuery("*:*"));
	}
}
