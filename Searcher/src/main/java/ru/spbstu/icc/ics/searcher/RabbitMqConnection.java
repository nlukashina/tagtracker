/*
Input message - SearchTagMessage, Output message - ShowTagMessage.

Properties for receiving messages:
queue - RabbitMqProperties.searcherReceiveQueueName
exchange - RabbitMqProperties.searchTagMessageExchangeName
routing-key - RabbitMqProperties.searchTagRoutingKey.

Properties for sending messages:
queue - RabbitMqProperties.serverReceiveQueueName
exchange - RabbitMqProperties.showTagMessageExchangeName
routing-key - RabbitMqProperties.showTagRoutingKey.
*/

package ru.spbstu.icc.ics.searcher;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.rabbitmq.client.ShutdownSignalException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import ru.spbstu.icc.ics.model.RabbitMqProperties;

class StaticConnectionFactory extends ConnectionFactory {
	
	public StaticConnectionFactory(String hostName) {
		super();
		try {
			setUri("amqp://195.208.117.141");
		} catch (KeyManagementException | NoSuchAlgorithmException
				| URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		setHost(hostName);
	}
}

public class RabbitMqConnection {
	
	// SHOW
	private final static String
		QUEUE_SHOW_SEND = RabbitMqProperties.serverReceiveQueueName;
	
	private final static String
		EXCHANGE_SHOW_SEND = RabbitMqProperties.showTagMessageExchangeName;
	
	private final static String
		ROUTING_KEY_SHOW_SEND = RabbitMqProperties.showTagRoutingKey;
	
	
	// UPLOAD
	/*
	private final static String
		QUEUE_UPLOAD_SEND = RabbitMqProperties.uploadedReceiveQueueName;
	*/
	
	private final static String
		EXCHANGE_UPLOAD_SEND = RabbitMqProperties.uploadDataMessageExchangeName;

	private final static String
		ROUTING_KEY_UPLOAD_SEND = RabbitMqProperties.uploadDataRoutingKey;

	
	/*
	private final static String
		QUEUE_UPLOAD_RECEIVE = RabbitMqProperties.uploadedReceiveQueueName;
	
	private final static String
		EXCHANGE_UPLOAD_RECEIVE = "process-uploaded-data-message-exchange";
	
	private final static String
		ROUTING_KEY_UPLOAD_RECEIVE = "process-uploaded-data-routing_key";
	*/
	
	
	// Visualize
	private final static String
		QUEUE_VISUALIZE_SEND = RabbitMqProperties.visualizerReceiveQueueName;
	
	private final static String
		EXCHANGE_VISUALIZE_SEND = RabbitMqProperties.visualizeDataMessageExchangeName;

	private final static String
		ROUTING_KEY_VISUALIZE_SEND = RabbitMqProperties.visualizeDataRoutingKey;
	
	
	// Analizer
	private final static String
		QUEUE_ANALYZE_RECEIVE = RabbitMqProperties.analyzerReceiveQueueName;
	
	private final static String
		EXCHANGE_ANALYZE_RECEIVE = RabbitMqProperties.analyzeTagMessageExchangeName;
	
	private final static String
		ROUTING_KEY_ANALYZE_RECEIVE = RabbitMqProperties.analyzeTagRoutingKey;
	
	
	// In progress
	private final static String
		QUEUE_IN_POGRESS_SEND = "analyze-progress-queue";
	
	private final static String
		EXCHANGE_IN_POGRESS_SEND = "analyze-progress-exchange";
	
	private final static String
		ROUTING_KEY_IN_POGRESS_SEND = "analyze-progress-routing-key";
	
	private StaticConnectionFactory factory;
	
	private Connection connection;
	private QueueingConsumer consumerUpload;
	private QueueingConsumer consumerShow;
	private QueueingConsumer consumerAnalyze;
	
	private Channel channelUploadSend;
	private Channel channelUploadReceive;
	
	private Channel channelShowSend;
	private Channel channelShowReceive;
	
	private Channel channelVisualizeSend;
	private Channel channelAnalyzeReceive;
	
	private Channel channelInProgressSend;
	
	public RabbitMqConnection(String host) {
		factory = new StaticConnectionFactory(host);
	}
	
	private void configurateShowSendingChannel() throws IOException {
		channelShowSend.queueDeclare(
			QUEUE_SHOW_SEND, false, false, false, null
		);
		
		channelShowSend.exchangeDeclare(
			EXCHANGE_SHOW_SEND, "direct"
		);
		
		channelShowSend.queueBind(
			QUEUE_SHOW_SEND, EXCHANGE_SHOW_SEND, ROUTING_KEY_SHOW_SEND
		);
	}
	
	/*
	private void configurateUploadSendingChannel() throws IOException {
		channelUploadSend.queueDeclare(
			QUEUE_UPLOAD_RECEIVE, false, false, false, null
		);
		//
		channelUploadSend.exchangeDeclare(
			EXCHANGE_UPLOAD_SEND, "direct"
		);
		
		channelUploadSend.queueBind(
			QUEUE_UPLOAD_SEND,
			EXCHANGE_UPLOAD_SEND, ROUTING_KEY_UPLOAD_SEND
		);
	}
	*/
	
	private void configurateVisualizeSendingChannel() throws IOException {
		channelVisualizeSend.queueDeclare(
			QUEUE_VISUALIZE_SEND, false, false, false, null
		);
		
		channelVisualizeSend.exchangeDeclare(
			EXCHANGE_VISUALIZE_SEND, "direct"
		);
		
		channelVisualizeSend.queueBind(
			QUEUE_VISUALIZE_SEND, EXCHANGE_VISUALIZE_SEND, ROUTING_KEY_VISUALIZE_SEND
		);
	}
	
	private void configurateInProgressSendingChannel() throws IOException {
		channelInProgressSend.queueDeclare(
			QUEUE_IN_POGRESS_SEND, false, false, false, null
		);
		
		channelInProgressSend.exchangeDeclare(
			EXCHANGE_IN_POGRESS_SEND, "direct"
		);
		
		channelInProgressSend.queueBind(
			QUEUE_IN_POGRESS_SEND,
			EXCHANGE_IN_POGRESS_SEND,
			ROUTING_KEY_IN_POGRESS_SEND
		);
	}
	
	/*
	private void configurateUploadReceivingChannel() throws IOException {
		//
		channelUploadReceive.queueDeclare(
			QUEUE_UPLOAD_RECEIVE, false, false, false, null
		);
		
		channelUploadReceive.exchangeDeclare(
			EXCHANGE_UPLOAD_RECEIVE, "direct"
		);
		
		channelUploadReceive.queueBind(
			QUEUE_UPLOAD_RECEIVE, EXCHANGE_UPLOAD_RECEIVE, ROUTING_KEY_UPLOAD_RECEIVE
		);
		
		consumerUpload = new QueueingConsumer(channelUploadReceive);
		channelUploadReceive.basicConsume(QUEUE_UPLOAD_RECEIVE, false, consumerUpload);
	}
	*/
	
	private void configurateAnalyzeReceivingChannel() throws IOException {
		channelAnalyzeReceive.queueDeclare(
			QUEUE_ANALYZE_RECEIVE, false, false, false, null
		);
		
		channelAnalyzeReceive.exchangeDeclare(
			EXCHANGE_ANALYZE_RECEIVE, "direct"
		);
		
		channelAnalyzeReceive.queueBind(
			QUEUE_ANALYZE_RECEIVE, EXCHANGE_ANALYZE_RECEIVE, ROUTING_KEY_ANALYZE_RECEIVE
		);
		
		
		consumerAnalyze = new QueueingConsumer(channelAnalyzeReceive);
		channelAnalyzeReceive.basicConsume(QUEUE_ANALYZE_RECEIVE, false, consumerAnalyze);
	}
	
	
	private boolean configurateChannels() {
		try {
			configurateShowSendingChannel();
			
//			configurateUploadSendingChannel();
//			configurateUploadReceivingChannel();
			
			configurateVisualizeSendingChannel();
			configurateAnalyzeReceivingChannel();
			
			configurateInProgressSendingChannel();
			
			
			
		} catch (IOException e) {
			System.out.println(
				"Cannot configurate channels.\n"
				+ e.fillInStackTrace()
			);
			
			return false;
		}
		
		return true;
	}
	
	public boolean tryConnect() {
		try {
			connection = factory.newConnection();
			
			channelShowSend = connection.createChannel();
			
//			channelUploadSend = connection.createChannel();
//			channelUploadReceive = connection.createChannel();
			
			channelVisualizeSend = connection.createChannel();
			channelAnalyzeReceive = connection.createChannel();
			
			channelInProgressSend = connection.createChannel();
		} catch (IOException e) {
			System.out.println(
				"Cannot connect to RabbitMQ."
				+ e.fillInStackTrace()
			);
			
			return false;
		}
		
		return configurateChannels();
	}
	
	public String getNextShowMessage() {
		try {
			Delivery deliveryShow = consumerShow.nextDelivery();
			String message = new String(deliveryShow.getBody());
			
			channelShowSend.basicAck(
				deliveryShow.getEnvelope().getDeliveryTag(), false
			);
			
			return message;
		} catch (ShutdownSignalException
			| ConsumerCancelledException
			| InterruptedException 
			| IOException e
		) {
			System.out.println("Failed to get next message. " + e.toString());
			return null;
		}
	}
	
	public String getNextUploadMessage() {
		try {
			Delivery deliveryUpload = consumerUpload.nextDelivery();
			String message = new String(deliveryUpload.getBody());
			
			channelUploadReceive.basicAck(
				deliveryUpload.getEnvelope().getDeliveryTag(), false
			);
			
//				System.out.println("Received message: " + message);
			return message;
		} catch (ShutdownSignalException
			| ConsumerCancelledException
			| InterruptedException 
			| IOException e
		) {
			System.out.println("Failed to get next message. " + e.toString());
		}
		return null;
	}
	
	public String getNextAnalyzeMessage() {
		String message = null;
		try {
			Delivery deliveryAnalyze = consumerAnalyze.nextDelivery();
			message = new String(deliveryAnalyze.getBody());
			
			channelAnalyzeReceive.basicAck(
				deliveryAnalyze.getEnvelope().getDeliveryTag(), false
			);
			
			System.out.println("Received message: " + message);
				
		} catch (ShutdownSignalException
			| ConsumerCancelledException
			| InterruptedException 
			| IOException e
		) {
			System.out.println("Failed to get next message. " + e.toString());
		}
		return message;
	}
	
	public boolean sendShowMessage(String message) {
		try {
			System.out.println("Send: " + message);
			channelShowSend.basicPublish(
				EXCHANGE_SHOW_SEND,
				ROUTING_KEY_SHOW_SEND,
				null, message.getBytes()
			);
			System.out.println("Send successful!");
		} catch (IOException e) {
			
			System.out.println(
				"Cannot send message!\n" + e.fillInStackTrace()
			);
			return false;
		}
		
		return true;
	}
	
	public boolean sendUploadMessage(String message) {
		try {
			System.out.println("Send: " + message);
			channelUploadSend.basicPublish(
				EXCHANGE_UPLOAD_SEND,
				ROUTING_KEY_UPLOAD_SEND,
				null, message.getBytes()
			);
			System.out.println("Send successful!");
		} catch (IOException e) {
			
			System.out.println(
				"Cannot send message!\n" + e.fillInStackTrace()
			);
			return false;
		}
		
		return true;
	}
	
	public boolean sendVisualizeMessage(String message) {
		try {
			System.out.println("Send: " + message);
			channelVisualizeSend.basicPublish(
				EXCHANGE_VISUALIZE_SEND,
				ROUTING_KEY_VISUALIZE_SEND,
				null, message.getBytes()
			);
			System.out.println("Send successful!");
		} catch (IOException e) {
			
			System.out.println(
				"Cannot send message!\n" + e.fillInStackTrace()
			);
			return false;
		}
		
		return true;
	}
	
	public boolean sendInProgressMessage(String message) {
		try {
			System.out.println("Send: " + message);
			channelInProgressSend.basicPublish(
				EXCHANGE_IN_POGRESS_SEND,
				ROUTING_KEY_IN_POGRESS_SEND,
				null, message.getBytes()
			);
			System.out.println("Send successful!");
		} catch (IOException e) {
			
			System.out.println(
				"Cannot send message!\n" + e.fillInStackTrace()
			);
			return false;
		}
		
		return true;
	}
	
	public int getCountMessagesShowReceive() {
		try {
			return channelShowReceive.queueDeclare().getMessageCount();
		} catch (IOException
			| com.rabbitmq.client.AlreadyClosedException e) {
			return -1;
		}
	}
	
	public int getCountMessagesUploadReceive() {
		try {
			return channelUploadReceive
				.queueDeclare()
				.getMessageCount();
		} catch (IOException
			| com.rabbitmq.client.AlreadyClosedException e) {
			return -1;
		}
	}
	
	public int getCountMessagesAnalyzeReceive() {
		try {
			return channelAnalyzeReceive.queueDeclare().getMessageCount();
		} catch (IOException
			| com.rabbitmq.client.AlreadyClosedException e) {
			return -1;
		}
	}
	
	public void closeAll() {
		try {
			channelShowSend.close();
			channelShowReceive.close();
			
			channelUploadSend.close();
			channelUploadReceive.close();
			
			channelVisualizeSend.close();
			channelAnalyzeReceive.close();
			
			connection.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

