package ru.spbstu.icc.ics.searcher;

//import java.net.URL;
//
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import org.apache.commons.io.IOUtils;
import org.apache.solr.client.solrj.SolrServerException;
//import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.params.ModifiableSolrParams;

import java.io.IOException;


public class Tests {
	public static void solrTest() {
		try {
//			String url = "http://pastebin.com/raw.php?i=AnSDjKKP";
//			String twJson = IOUtils.toString(new URL(url).openStream());
//			ObjectMapper mapper = new ObjectMapper();
			SolrConnection solr = new SolrConnection();
//			JsonNode node = mapper.readTree(twJson);

//			solr.indexJSON(node);
			ModifiableSolrParams params = new ModifiableSolrParams();
			params.set("fl", "tag");

			params.set("q", "tag:*");
			System.out.println(solr.searchByQuery(params));
			params.set("q", "tag:first*");
			System.out.println(solr.searchByQuery(params));
			params.set("q", "tag:firste*");
			System.out.println(solr.searchByQuery(params));

			System.out.println(solr.searchByQuery("lolik-1"));
			System.out.println(solr.countByQuery("lolik-1"));

			System.out.println(solr.deleteByQuery("*:*"));
		} catch (IOException | SolrServerException e) {
			e.printStackTrace();
		}
	}
	
	public static void rabbitMqTest() {
		RabbitMqConnection rabbitMQConnection = 
			new RabbitMqConnection("asus-n56v");
		
		System.out.println("Attempting to connect to RabbitMQ...");
		if(rabbitMQConnection.tryConnect()) {
			System.out.println("Successful connection.");
		}
		else {
			System.out.println("Failed connection.");
			return;
		}
		
		// по идее я здесь должен принять сообщения
		// вывести
		// отправить Success + номер сообщения
		
		String receiveMessage;
		int countReceiveMessages = 0;
		boolean run = true;
		boolean stopByTimeout = false;
		while(run) {
			
			if(!stopByTimeout) {
			
				receiveMessage = rabbitMQConnection.getNextUploadMessage();
				if(receiveMessage == null || receiveMessage.isEmpty()) {
					try {
						System.out.println("Waiting messages...");
						Thread.sleep(5000);
						stopByTimeout = true;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						continue;
					}
				}
				else {
					countReceiveMessages++;
					System.out.println(
						countReceiveMessages + "\n"
						+ receiveMessage
					);
					
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						continue;
					}
					
					rabbitMQConnection.sendShowMessage(
						"Success " + countReceiveMessages
					);
				}
			} else {
				receiveMessage = rabbitMQConnection.getNextUploadMessage();
				if(receiveMessage == null || receiveMessage.isEmpty()) {
					run = false;
					rabbitMQConnection.closeAll();
					System.out.println("No messages.");
				} else {
					countReceiveMessages++;
					
					System.out.println(
						countReceiveMessages + "\n"
						+ receiveMessage);
					rabbitMQConnection.sendShowMessage(
						"Success " + countReceiveMessages
					);
					
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						continue;
					}
					
					stopByTimeout = false;
				}
			}
		}
	}
}
