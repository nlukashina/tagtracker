package ru.spbstu.icc.ics.searcher.test;

/*

Input message - SearchTagMessage, Output message - ShowTagMessage.

Properties for receiving messages:
queue - RabbitMqProperties.searcherReceiveQueueName
exchange - RabbitMqProperties.searchTagMessageExchangeName
routing-key - RabbitMqProperties.searchTagRoutingKey.

Properties for sending messages:
queue - RabbitMqProperties.serverReceiveQueueName
exchange - RabbitMqProperties.showTagMessageExchangeName
routing-key - RabbitMqProperties.showTagRoutingKey.
*/

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.rabbitmq.client.ShutdownSignalException;

import ru.spbstu.icc.ics.model.RabbitMqProperties;

class StaticConnectionFactory extends ConnectionFactory {
	
	public StaticConnectionFactory(String hostName) {
		super();
		setHost(hostName);
	}
}

public class RabbitMqTestConnection {
	
	private final static String
		QUEUE_SEND = RabbitMqProperties.searcherReceiveQueueName;
	
	private final static String
		EXCHANGE_SEND = RabbitMqProperties.searchTagMessageExchangeName;
	
	private final static String
		ROUTING_KEY_SEND = RabbitMqProperties.searchTagRoutingKey;

	
	private final static String
		QUEUE_RECEIVE = RabbitMqProperties.serverReceiveQueueName;

	private final static String
		EXCHANGE_RECEIVE = RabbitMqProperties.showTagMessageExchangeName;
	
	private final static String
		ROUTING_KEY_RECEIVE = RabbitMqProperties.showTagRoutingKey;


	private StaticConnectionFactory factory;
	
	private Connection connection;
	private QueueingConsumer consumer;
	private Channel channelSend;
	private Channel channelReceive;
	private Delivery delivery;
	
	public RabbitMqTestConnection(String host) {
		factory = new StaticConnectionFactory(host);
	}
	
	private void configurateSendingChannel() throws IOException {
		channelSend.queueDeclare(
			QUEUE_SEND, false, false, false, null
		);
		
		channelSend.exchangeDeclare(
			EXCHANGE_SEND, "direct"
		);
		
		channelSend.queueBind(
			QUEUE_SEND, EXCHANGE_SEND, ROUTING_KEY_SEND
		);
	}
	
	private void configurateReceivingChannel() throws IOException {
		channelReceive.queueDeclare(
			QUEUE_RECEIVE, false, false, false, null
		);
		
		channelReceive.exchangeDeclare(
			EXCHANGE_RECEIVE, "direct"
		);
		
		channelReceive.queueBind(
			QUEUE_RECEIVE, EXCHANGE_RECEIVE, ROUTING_KEY_RECEIVE
		);
		
		consumer = new QueueingConsumer(channelReceive);
		channelReceive.basicConsume(QUEUE_RECEIVE, false, consumer);
	}
	
	private boolean configurateChannels() {
		try {
			configurateSendingChannel();
			configurateReceivingChannel();
		} catch (IOException e) {
			System.out.println(
				"Cannot configurate channels.\n"
				+ e.fillInStackTrace()
			);
			
			return false;
		}

		return true;
	}
	
	public boolean tryConnect() {
		try {
			connection = factory.newConnection();
			channelSend = connection.createChannel();
			channelReceive = connection.createChannel();
		} catch (IOException | TimeoutException e) {
			System.out.println(
				"Cannot connect to RabbitMQ."
				+ e.fillInStackTrace()
			);
			
			return false;
		}
		
		return configurateChannels();
	}
	
	public String getNextMessage() {
		try {
			delivery = consumer.nextDelivery();
			String message = new String(delivery.getBody());
			channelReceive.basicAck(
				delivery.getEnvelope().getDeliveryTag(), false
			);
			
			return message;
		} catch (ShutdownSignalException
			| ConsumerCancelledException
			| InterruptedException
			| IOException e
		) {
			System.out.println("Failed to get next message.");
			return null;
		}
	}
	
	public boolean sendMessage(String message) {
		try {
			channelSend.basicPublish(
				EXCHANGE_SEND,
				ROUTING_KEY_SEND,
				null, message.getBytes()
			);
		} catch (IOException e) {
			
			System.out.println(
				"Cannot send message!\n" + e.fillInStackTrace()
			);
			return false;
		}
		
		return true;
	}
	
	public void closeAll() {
		try {
			channelSend.close();
			channelReceive.close();
			connection.close();
		} catch (IOException | TimeoutException e) {
			// TODO Auto-generated catch block
			System.out.println(
				"Failed to close channels and connection." + e.fillInStackTrace()
			);
		}
	}
}

