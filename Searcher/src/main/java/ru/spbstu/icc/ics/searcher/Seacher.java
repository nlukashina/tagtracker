/*
Как всё это будет работать.
1. Если в ваш модуль приходит сообщение
ProcessUploadedDataMessage, содержащее
данные из твиттера, вы должны выбрать
из него все тэги и сохранить их к себе
(в индекс, т.е. проиндексировать для дальнейшего использования).
С ними вы будете дальше работать при поиске.

Теоретически, эти сообщения придут к вам одной
большой серией в начале работы - когда модуль
загрузки загрузит кучу данных.

2. Если в ваш модуль приходит сообщение
SearchTagMessage, вы должны выполнить поиск
индексированных тегов
(т.е. найти все, которые начинаются так, как указано в сообщении)
и вернуть их в сообщении ShowTagMessage.
Всё это соответствует функции автодоплнения на UI.


Задачи для Димы:
1. https://bitbucket.org/nlukashina/tagtracker/issues/12..
В рамках этой задачи нужно "подрубить" Solr к проекту
и настроить его так, как вам будет нужно.

2. https://bitbucket.org/nlukashina/tagtracker/issues/14..
В рамках этой задачи нужно, собственно,
выполнить поиск по заданному тегу.
(Сходить в солр с нужным запросом, найти все теги
и сформировать сообщение для ответа).


Задачи для Антона:
1. https://bitbucket.org/nlukashina/tagtracker/issues/13..
В рамках этой задачи нужно обеспечить взаимодействие
модулей через раббит.

Т.е. сделать так, чтобы модуль получал и отправлял
нужные сообщения другим модулям. Все нужные данные
для Раббита описаны в задаче.

В рамках этой же задачи нужно класть полученные данные
в солр (индексировать нужные поля).
*/
package ru.spbstu.icc.ics.searcher;

import java.io.IOException;
//import java.util.Iterator;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.params.ModifiableSolrParams;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class Seacher {
	
//	private static final String START_UPLOAD_MESSAGE = "{type:1}";
//	private static final String FINISH_UPLOAD_MESSAGE = "{\"type\":2}";
	private static final String IN_PROGRESS_MESSAGE = "{\"type\":9}";
	
	private RabbitMqConnection rabbitConnection;
	
	private SolrConnection solrConnection;
	private ModifiableSolrParams params;
	
	private ObjectMapper mapper;
	
	private Gson gson;
	
	public Seacher() {
		rabbitConnection = new RabbitMqConnection("asus-n56v");
		
		solrConnection = new SolrConnection();
		params = new ModifiableSolrParams();
		
		mapper = new ObjectMapper();
	}
	
	private String parseInputShowTag(JsonNode node) {
		String messageOut = "";
		try {
			String hashtag = node.get("tag").asText();
			
			System.out.println("Seaching tag: \"" + hashtag + "*\"");
			System.out.println(
				"Param for search -> " + "tag:" + hashtag + "*"
			);
			
			messageOut = "{"
					+ "\n\t" + "\"type\":" + 6 + ","
					+ "\n\t" + solrConnection.searchByQuery2(hashtag + "*")
					+ "\n" + "}";
			
			params.clear();
			
		} catch (SolrServerException | IOException e) {
			
			messageOut = 
					"{"
					+ "\n\t" + "\"type\":" + 6 + ","
					+ "\n\t" +"\"tags\":" + "[]"
					+ "\n" + "}";
			
		}
		System.out.println("Result:\n" + messageOut);
		
		return messageOut;
	}
	
	private String parseInputCountTag(JsonNode node) {
		
		String tag = node.get("tag").asText();
		System.out.println("Count tweets with tag: \"" + tag + "\"");
		String messageOut = 
			"{"
			+ "\n\t\"type\":" + 8 + ','
			+ "\n\t\"count\":";
		
		try {
			messageOut += solrConnection.countByQuery(tag);
		} catch (SolrServerException | IOException e) {
			messageOut += 0;
		}
		
		messageOut += "\n}";
		System.out.println("Result:\n" + messageOut);
		
		return messageOut;
	}
	
	/*
	private void runUpload() {
		boolean getTweets = true;
		if(getTweets) {
			// getting messages from uploader
			if(!rabbitConnection.sendUploadMessage(START_UPLOAD_MESSAGE))
				return;
			
			String message = rabbitConnection.getNextUploadMessage();
			while(message != null && !message.equals(FINISH_UPLOAD_MESSAGE)) {
				
				if(message != null && !message.equals(START_UPLOAD_MESSAGE)) {
					try {
						JsonNode node = mapper.readTree(message);
						
						System.out.println("TYPE 2: UPPLOAD_DATA");
						try {
							solrConnection.indexJSON(node);
						} catch (SolrServerException e) {
							// TODO Auto-generated catch block
							System.out.println("Cannot index receive message.");
						}
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					} catch (IOException e) {
						System.out.println("Cannot parse receive message.");
					}
				}
				if(!rabbitConnection.sendUploadMessage(START_UPLOAD_MESSAGE))
					return;
				message = rabbitConnection.getNextUploadMessage();
			}
			System.out.println("Finish!");
			
			getTweets = false;
		}
			
	}
	*/
	
	private String analyze(String message) {
//		String message = rabbitConnection.getNextAnalyzeMessage();
//		String message = "{\"type\":3,"
//			+ "\"tag\":\"job\","
//			+ "\"analyze_type\":"+ i +"}";
		Map<String, Integer> map = null;
		if(message != null) {
			try {
				JsonNode node = mapper.readTree(message);
				String tag = node.get("tag").asText();
				Integer analyzeType = node.get("analyze_type").asInt();
				
				switch (analyzeType) {
				case 1: // Распределение твитов с заданным тегом по дням недели
					map = solrConnection.countByDate(
						tag,   "Mon", "Tue", "Wed",
						"Thu", "Fri", "Sat", "Sun"
					);
					break;

				case 2: // По странам: Russia, USA, Norway, Ukraine
					map = solrConnection.countByCountry(
						tag, "Russia", "United States", "Canada", "null"
					);
				break;

				case 3: // По количеству ретвитов: 
					map = solrConnection.countByRT(
						tag,
						1,  10,  10,  50,
						50, 100, 100, 200
					);
					break;

				case 4: // По количеству like:
					map = solrConnection.countByFavourite(
						tag,
						1,  10,  10,  50,
						50, 100, 100, 200
					);
					break;

				case 5: // По языку:
					map = solrConnection.countByLang(
						tag, "ru", "en", "de", "ua");
					break;

				case 6: // По количеству followers у юзера:
					map = solrConnection.countByFollowers(
						tag,
						1,  10,  10,  50,
						50, 100, 100, 200
					);
					break;

				default:
					break;
				}
				
				if(map != null) {
					return gson.toJson(map)
						+ ",\"analyze_type\":" + analyzeType;
				} else {
					return "";
				}
				
			} catch(IOException e) {
				
			} catch (SolrServerException e) {
				System.out.println("Cannot get info from Solr.");
			}
		}
		return "";
	}
	
	public void run() {
		
		System.out.println("Attempting to connect to RabbitMQ...");
		if(rabbitConnection.tryConnect()) {
			System.out.println("Successful connection.");
		}
		else {
			System.out.println("Failed connection.");
			return;
		}
		
		gson = new Gson();
		
		boolean start = true;
		while(start) {
			
//			runUpload();
//			System.out.println(
//				rabbitConnection.getCountMessagesShowReceive()
//			);
			
			/* AnalyzeTagMessage
			 * {
			 *     "type": 3,
			 *     "tag": "lolik",
			 *     "analyze_type": 3
			 * }
			*/
			
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			
			String message = rabbitConnection.getNextAnalyzeMessage();
			System.out.println("Received meesage:\n" + message);
			
			try {
				JsonNode node = mapper.readTree(message);
				JsonNode type = node.get("type");
				
				switch (type.asInt()) {
				case 3:
					String jsonMap = analyze(message);
					if(!jsonMap.isEmpty()) {
						System.out.println(jsonMap);
						rabbitConnection.sendVisualizeMessage(
							"{\"type\":4,"
							+ "\"map\":" + jsonMap + "}"
						);
						rabbitConnection.sendInProgressMessage(
							IN_PROGRESS_MESSAGE
						);
					}
					break;
				case 5: // SHOW_TAG_MESSAGE
					System.out.println("TYPE 5: SHOW_TAG");
					rabbitConnection.sendShowMessage(parseInputShowTag(node));
					break;
					
				case 7: // COUNT_TAG_MESSAGE
					System.out.println("TYPE 7: COUNT_TAG");
					rabbitConnection.sendShowMessage(parseInputCountTag(node));
					
					break;
					
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NullPointerException e1) {
				continue;
			}

		}
	}
}
