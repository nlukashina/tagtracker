package ru.spbstu.icc.ics.searcher;

import java.io.IOException;
import java.util.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;

import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.util.NamedList;

import static java.lang.Math.toIntExact;

public class SolrConnection {
	HttpSolrClient client;
	SolrInputDocument document;
	SolrQuery solrQuery;
	Gson gson;
	
	Integer countDocuments;
	
	private String parseTagResponse(String jsonResponse) {
		jsonResponse = jsonResponse.replace("]},{\"tag\":[", ",");
		jsonResponse = jsonResponse.replace("[{", "");
		jsonResponse = jsonResponse.replace("}]", "");
		jsonResponse = jsonResponse.replace("tag", "hashtags");
		return jsonResponse;
	}
	
	public SolrConnection() {
		client = new HttpSolrClient("http://localhost:8983/solr/tag_tracker");
		document = new SolrInputDocument();
		
		solrQuery = new SolrQuery();
		solrQuery.setHighlight(true);
		solrQuery.setHighlightRequireFieldMatch(true);
		
		gson = new Gson();
		countDocuments = 0;
	}

	public SolrConnection(String url) {
		client = new HttpSolrClient(url);
		document = new SolrInputDocument();
	}

	public void indexJSON(JsonNode node) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		
		System.out.println("\t\tAdding to index");

		Iterator<JsonNode> tweets = node.path("tweets").iterator();
		while (tweets.hasNext()) {
			SolrInputDocument document = new SolrInputDocument();
			JsonNode current = tweets.next();
			document.addField("text", current.path("text").asText());
			document.addField("lang", current.path("lang").asText());
			document.addField("createdAt", current.path("createdAt").asText());
			
			countDocuments = countDocuments + 1;
			
			System.out.println(
				"Adding new document:" +
				"\n\ttext = "      + current.path("text").asText() +
				"\n\tlang = "      + current.path("lang").asText() +
				"\n\tcreatedAt = " + current.path("createdAt").asText() +
				"\n\thashtags :"
			);
			
			Iterator<JsonNode> hashtags = current.path("hashtags").iterator();
			while (hashtags.hasNext()) {
				String c = hashtags.next().path("text").asText();
				
				System.out.println("\t\t\"" + c + "\""); 
				
				document.addField("tag", c);
			}

			document.addField("country", current.path("country").asText());
			document.addField("followers_count", current.path("followers_count").asInt());
			document.addField("retweet_count", current.path("retweet_count").asInt());
			document.addField("favourite_count", current.path("favorite_count").asText());
			
			System.out.println(
				"\tcountry = "         + current.path("country").asText() +
				"\n\tfollowers_count = " + current.path("followers_count").asText() +
				"\n\tretweet_count = "   + current.path("retweet_count").asText() +
				"\n\tfavourite_count = " + current.path("favorite_count").asText()
			);
			
			client.add(document);
		}
		System.out.println("Commiting...");
		
		client.commit();
		
		System.out.println("Commit successful!\nCount Documents = " + countDocuments + "\n");
	}
	public long countByCountry(String country) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "country:"+country);
		params.set("fl", "country");
		QueryResponse qr = client.query(params);
		return qr.getResults().getNumFound();
	}

	public long countByLang(String i1) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "lang:"+i1);
		params.set("fl", "lang");
		QueryResponse qr = client.query(params);
		return qr.getResults().getNumFound();
	}

	public long countByRT(int start, int end) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "retweet_count:["+ Integer.toString(start) + " TO " + Integer.toString(end) + "]");
		params.set("fl", "retweet_count");
		QueryResponse qr = client.query(params);
		return qr.getResults().getNumFound();
	}

	public long countByFollowers(int start, int end) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "follower_count:["+ Integer.toString(start) + " TO " + Integer.toString(end) + "]");
		params.set("fl", "follower_count");
		QueryResponse qr = client.query(params);
		return qr.getResults().getNumFound();
	}

	public long countByFavourite(int start, int end) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "favourite_count:["+ Integer.toString(start) + " TO " + Integer.toString(end) + "]");
		params.set("fl", "favourite_count");
		QueryResponse qr = client.query(params);
		return qr.getResults().getNumFound();
	}

	public boolean indexNamedTag(ArrayList<String> tags) {
		
		if(tags.isEmpty()) {
			System.out.println("No tags.\n");
			return false;
		}
		
		Iterator<String> it = tags.iterator();
		System.out.println("Indexing...");
		try {
			while(it.hasNext()) {
				document.addField("tag", it.next());
				client.add(document);
				
				document.clear();
			}
			client.commit();
			System.out.println("Success!\n");
		} catch (SolrServerException | IOException e) {
			System.out.println("Failed.\n");
		}
		
		tags.clear();
		
		return true;
	}

	@SuppressWarnings("unchecked")
	public int countByQuery(String tag) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		SolrQuery qu = new SolrQuery();
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "tag:"+query);
		params.set("fl", "tag");
		QueryResponse qr = client.query(params);
		return qr.getResults().getNumFound();
	}

	public String searchByQuery(String query) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", query);
		params.set("fl", "tag");
		QueryResponse qr = client.query(params);
		
		return parseTagResponse(gson.toJson(qr));
	}

	public String searchByQuery(ModifiableSolrParams params) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		SolrDocumentList qr = client.query(params).getResults();
		
		return parseTagResponse(gson.toJson(qr));
	}

	public String searchByQuery2(String query) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {

		ModifiableSolrParams params = new ModifiableSolrParams();
		String addQuery = query;
		String retValue = "\"tags\": [";
		int i;
		params.set("fl", "tag");
		params.set("hl", "on");
		params.set("hl.fl", "tag");

		for (i=0;i<10;i++) {
			params.set("q", "tag:" + addQuery);
			QueryResponse qr = client.query(params);
			System.out.print(qr.getHighlighting());
			if (!qr.getHighlighting().isEmpty()) {
				String foundTag = qr.getHighlighting().values().iterator().next().values().iterator().next().iterator().next();
				foundTag = foundTag.substring(4, foundTag.length()-5);
				System.out.print("\n !!! " + foundTag + "\n");

				if (i==0) {retValue += "\"" + foundTag + "\"";}
				else {retValue += ", \"" + foundTag + "\"";}

				addQuery += (" AND NOT tag:" + foundTag);
			}
			else break;
		}

		retValue += "]";
		return retValue;
	}

	public Map<String, Integer> countByDate(String tag, String i1, String i2, String i3, String i4, String i5, String i6, String i7) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		Map<String, Integer> returnValue = new HashMap<String, Integer>();
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "createdAt:"+i1 + "* AND tag:" + tag);
		params.set("fl", "createdAt");
		QueryResponse qr = client.query(params);
		returnValue.put(i1, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "createdAt:"+i2+ "* AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i2, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "createdAt:"+i3+ "* AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i3, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "createdAt:"+i4+ "* AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i4, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "createdAt:"+i5+ "* AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i5, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "createdAt:"+i6+ "* AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i6, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "createdAt:"+i7+ "* AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i7, toIntExact(qr.getResults().getNumFound()));
		return returnValue;
	}

	public Map<String, Integer> countByCountry(String tag, String i1, String i2, String i3, String i4) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		Map<String, Integer> returnValue = new HashMap<String, Integer>();
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "country:"+i1 + " AND tag:" + tag);
		params.set("fl", "country");
		QueryResponse qr = client.query(params);
		returnValue.put(i1, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "country:"+i2+ " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i2, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "country:"+i3+ " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i3, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "country:"+i4+ " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i4, toIntExact(qr.getResults().getNumFound()));
		return returnValue;
	}

	public Map<String, Integer> countByLang(String tag, String i1, String i2, String i3, String i4) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		Map<String, Integer> returnValue = new HashMap<String, Integer>();
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("q", "lang:"+i1+ " AND tag:" + tag);
		params.set("fl", "lang");
		QueryResponse qr = client.query(params);
		returnValue.put(i1, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "lang:"+i2 + " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i2, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "lang:"+i3+ " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i3, toIntExact(qr.getResults().getNumFound()));
		params.set("q", "lang:"+i4+ " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put(i4, toIntExact(qr.getResults().getNumFound()));
		return returnValue;
	}

	public Map<String, Integer> countByRT(String tag, int s1, int e1, int s2, int e2, int s3, int e3, int s4, int e4) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		Map<String, Integer> returnValue = new HashMap<String, Integer>();
		params.set("q", "retweet_count:["+ Integer.toString(s1) + " TO " + Integer.toString(e1) + "]" + " AND tag:" + tag);
		params.set("fl", "retweet_count");
		QueryResponse qr = client.query(params);
		returnValue.put("["+ Integer.toString(s1) + " TO " + Integer.toString(e1) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "retweet_count:["+ Integer.toString(s2) + " TO " + Integer.toString(e2) + "]"+ " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s2) + " TO " + Integer.toString(e2) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "retweet_count:["+ Integer.toString(s3) + " TO " + Integer.toString(e3) + "]"+ " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s3) + " TO " + Integer.toString(e3) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "retweet_count:["+ Integer.toString(s4) + " TO " + Integer.toString(e4) + "]"+ " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s4) + " TO " + Integer.toString(e4) + "]", toIntExact(qr.getResults().getNumFound()));
		return returnValue;
	}

	public Map<String, Integer> countByFavourite(String tag, int s1, int e1, int s2, int e2, int s3, int e3, int s4, int e4) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		Map<String, Integer> returnValue = new HashMap<String, Integer>();
		params.set("q", "favourite_count:["+ Integer.toString(s1) + " TO " + Integer.toString(e1) + "]" + " AND tag:" + tag);
		params.set("fl", "favourite_count");
		QueryResponse qr = client.query(params);
		returnValue.put("["+ Integer.toString(s1) + " TO " + Integer.toString(e1) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "favourite_count:["+ Integer.toString(s2) + " TO " + Integer.toString(e2) + "]" + " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s2) + " TO " + Integer.toString(e2) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "favourite_count:["+ Integer.toString(s3) + " TO " + Integer.toString(e3) + "]" + " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s3) + " TO " + Integer.toString(e3) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "favourite_count:["+ Integer.toString(s4) + " TO " + Integer.toString(e4) + "]" + " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s4) + " TO " + Integer.toString(e4) + "]", toIntExact(qr.getResults().getNumFound()));
		return returnValue;
	}

	public Map<String, Integer> countByFollowers(String tag,int s1, int e1, int s2, int e2, int s3, int e3, int s4, int e4) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		Map<String, Integer> returnValue = new HashMap<String, Integer>();
		params.set("q", "followers_count:["+ Integer.toString(s1) + " TO " + Integer.toString(e1) + "]" + " AND tag:" + tag);
		params.set("fl", "followers_count");
		QueryResponse qr = client.query(params);
		returnValue.put("["+ Integer.toString(s1) + " TO " + Integer.toString(e1) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "followers_count:["+ Integer.toString(s2) + " TO " + Integer.toString(e2) + "]" + " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s2) + " TO " + Integer.toString(e2) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "followers_count:["+ Integer.toString(s3) + " TO " + Integer.toString(e3) + "]" + " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s3) + " TO " + Integer.toString(e3) + "]", toIntExact(qr.getResults().getNumFound()));
		params.set("q", "followers_count:["+ Integer.toString(s4) + " TO " + Integer.toString(e4) + "]" + " AND tag:" + tag);
		qr = client.query(params);
		returnValue.put("["+ Integer.toString(s4) + " TO " + Integer.toString(e4) + "]", toIntExact(qr.getResults().getNumFound()));
		return returnValue;
	}


	public String deleteByQuery(String query) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
		UpdateResponse qr = client.deleteByQuery(query);
		client.commit();
		return qr.toString();
	}
}
