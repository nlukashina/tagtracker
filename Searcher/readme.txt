� ������ solr � ��������� ���������� tag_tracker ��� ������ �������.
����� ��������� solr ����� �������, � ����������� curl; ����� ����� ������������ ��������� �������:

1) cd solr-5.3.1\solr-5.3.1\bin
2) solr start // ����� ����� �� localhost:8983 (�� ���������) ����������� solr
3) curl "http://localhost:8983/solr/tag_tracker/update?commit=true" �data-binary @"�����json" -H "Content-type: application/json" - ���������� ������ json-a
4) curl "http://localhost:8983/solr/tag_tracker/select?q=*:*" - ������ �� ����� ���� ������� � �������; � ����� ������ �����, ��������, ������� ������ "http://localhost:8983/solr/tag_tracker/select?q=text:*#hashtag *"&fl=text - ������ � ����� ������ ���� text �����, � ������� ���� ��������� ���

������������� �������� ����������, ��� solr �� ������������ ��������� ��������� �������; ��������� ���������� ���������� json � ���� ������, �������� ������ ��, ��� ����� ��� ��������; �������� �� json-� ���� � ������������� �� �� ������. ���� ����������:
{
 "id" : x
 "hashtag" : "text"
}
"http://localhost:8983/solr/tag_tracker/select?q=hashtag:(��������� �����)*&fl=hashtag" � ������ � ����� json, � ���� response->docs �������� ����� ������ �����, ���������� ��� ������.