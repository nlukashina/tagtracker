package ru.spbstu.icc.ics.service;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.spbstu.icc.ics.message.UploadDataMessage;
import ru.spbstu.icc.ics.model.Tweet;

import java.util.List;

/**
 * Consumer for {@link UploadDataMessage}
 * Created by Nina Lukashina on 06.10.2015.
 */
@Component public class UploadDataMsgConsumer implements MessageListener {

    @Autowired private TwitterService twitterService;
    @Autowired private ProcessUploadedDataProducer processUploadedDataProducer;

    public void onMessage(Message message) {
        System.out.println(message);

        //TODO: List<Tweet> tweets = twitterService.uploadData();
        //TODO: ProcessUploadedDataMessage message = new ProcessUploadedDataMessage(tweets);
        //TODO: processUploadedDataProducer.sendMessage(tweets);
    }
}
