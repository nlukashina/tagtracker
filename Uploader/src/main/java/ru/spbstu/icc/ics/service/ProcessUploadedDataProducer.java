package ru.spbstu.icc.ics.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.spbstu.icc.ics.message.ProcessUploadedDataMessage;
import ru.spbstu.icc.ics.model.RabbitMqProperties;

/**
 * Producer which send {@link ProcessUploadedDataMessage}
 * Created by Nina Lukashina on 06.10.2015.
 */
@Component public class ProcessUploadedDataProducer {
    @Autowired private RabbitTemplate rabbitTemplate;

    public void sendMessage(ProcessUploadedDataMessage message) {
        rabbitTemplate.convertAndSend(RabbitMqProperties.processUploadedDataMessageExchangeName,
            RabbitMqProperties.processUploadedDataRoutingKey, message);
    }

}
