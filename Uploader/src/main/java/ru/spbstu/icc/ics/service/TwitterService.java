package ru.spbstu.icc.ics.service;

import org.springframework.stereotype.Service;
import ru.spbstu.icc.ics.model.Tweet;

import java.util.Collections;
import java.util.List;

/**
 * Service which provides methods to interact with Twitter's REST API.
 * <p>
 * Created by Nina Lukashina on 06.10.2015.
 */
@Service public class TwitterService {

    /**
     * Method upload data (tweets) from Twitter.
     *
     * @return list of tweets
     */
    public List<Tweet> uploadData() {
        //TODO: implement loading data from Twitter using its REST API.
        return Collections.emptyList();
    }
}
