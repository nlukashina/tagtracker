package ru.spbstu.icc.ics;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import ru.spbstu.icc.ics.message.UploadDataMessage;
import ru.spbstu.icc.ics.model.RabbitMqProperties;
import ru.spbstu.icc.ics.service.UploadDataMsgConsumer;

/**
 * Main class of module.
 * <p>
 * Created by Nina Lukashina on 06.10.2015.
 */
@SpringBootApplication public class UploaderApplication implements CommandLineRunner {
    @Autowired AnnotationConfigApplicationContext context;
    @Autowired private UploadDataMsgConsumer uploadDataMsgConsumer;
    @Autowired private RabbitTemplate rabbitTemplate;

    @Bean Queue queue() {
        return new Queue(RabbitMqProperties.uploadedReceiveQueueName, false);
    }

    @Bean TopicExchange exchange() {
        return new TopicExchange(RabbitMqProperties.uploadDataMessageExchangeName);
    }

    @Bean Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange)
            .with(RabbitMqProperties.uploadDataRoutingKey);
    }

    @Bean SimpleMessageListenerContainer container(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(RabbitMqProperties.uploadedReceiveQueueName);
        container.setMessageListener(uploadDataMsgConsumer);
        return container;
    }

    public static void main(String[] args) {
        System.out.println("Run....");
        ConfigurableApplicationContext ctx = SpringApplication.run(UploaderApplication.class, args);
    }

    public void run(String... args) throws Exception {
        rabbitTemplate.convertAndSend(RabbitMqProperties.processUploadedDataMessageExchangeName,
            RabbitMqProperties.processUploadedDataRoutingKey, "Hello from uploader module!");
        while (true) {
            Thread.sleep(5000);
            System.out.println("Uploader: Waiting for messages...");
            context.close();
        }
    }
}
