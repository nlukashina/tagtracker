// MongoDB name and address for connecting
export const mongo = {
	db: 'twitter',
	collection: 'data',
	ip: '127.0.0.1',
	port: '27017',
};

// RabbitMQ name and address for connecting
export const rabbit = {
	ip: '127.0.0.1',
	port: '5672',
	receive: {
		message: '{type:1}',
		exchange: 'upload-data-message-exchange',
		queue: 'uploader-receive-queue',
		key: 'upload-data-routing_key',
	},
	send: {
		exchange: 'process-uploaded-data-message-exchange',
		key: 'process-uploaded-data-routing_key',
	},
	msg_count: 50,  // number of tweets in every publish
};
